package com.mitocode.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.SignosVitales;
import com.mitocode.service.ISignosVitalesService;


@RestController
@RequestMapping("/signosvitales")
public class SignosVitalesController {
	
	@Autowired
	private ISignosVitalesService service;
	
	@GetMapping
	public ResponseEntity<List<SignosVitales>> listar() throws Exception{
		List<SignosVitales> lista = service.listar();
		return new ResponseEntity<List<SignosVitales>>(lista, HttpStatus.OK);		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SignosVitales> listarPorId(@PathVariable("id") Integer id) throws Exception{
		SignosVitales obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		
		return new ResponseEntity<SignosVitales>(obj, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<SignosVitales> registrar(@Valid @RequestBody SignosVitales p) throws Exception{
		SignosVitales obj = service.registrar(p);
		return new ResponseEntity<SignosVitales>(obj, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<SignosVitales> modificar(@Valid @RequestBody SignosVitales p) throws Exception{
		SignosVitales obj = service.modificar(p);
		return new ResponseEntity<SignosVitales>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception{
		SignosVitales obj = service.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
